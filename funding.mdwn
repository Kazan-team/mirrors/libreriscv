We currently have a bit over 350k in funding from NLNet for 2020.

We are always open to more funding provided that the funds align well
with our vision.

[NLNet](http://nlnet.nl) is our primary donor: Purism also are one of
our sponsors.  NLNet is a Registered Charity, meaning that if you fund
us through them it is tax-deductible to you or your business.

[Opencollective page](https://opencollective.com/libre-soc)

Decred:

* Current address (last changed 2023-01-30):

  ```
  DsT86SJHNWrjouPs2cZvXnTvrPgU84GCCoP
  ```

* Old address(es) (do *NOT* send money to them) can be retrieved
  from the [Git history of this page](https://git.libre-soc.org/?p=libreriscv.git;a=history;f=funding.mdwn;hb=HEAD).

Contact lkcl@lkcl.net for more information.

