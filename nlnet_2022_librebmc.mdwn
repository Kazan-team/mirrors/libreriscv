# NL.net proposal

## Project name

LibreBMC

## Website / wiki 

<https://libre-soc.org/nlnet_2022_librebmc>

## NLNet proposal 1 : LibreBMC Board Porting

__Abstract__: LibreBMC replaces the proprietary Baseboard Management
Controller (BMC) and its secret hidden firmware, entirely. In servers
typically used in Data Centre's and for scenarios where data privacy is
paramount this turns out to be critical. One of the most commonly-used
BMC Processors in the world has a silicon-baked plaintext password
for its Serial Console, and with a BMC being the very means by which a
processor's BIOS is uploaded, this publicly-available password allows for
a full bypass of every conceivable security measure. BMC (out-of-band)
Processors are also present in every AMD and Intel desktop and laptop
in the world, think the Intel Management Engine. Even replacing the
BIOS with coreboot is not enough to gain trust because the BMC is in
charge of uploading coreboot/oreboot, and could easily alter it. At
least in this case if the BMC's firmware is replaced it increases trust
that the payload (coreboot/oreboot) has not been tampered with. However
this is so low-level that there is serious risk of damaging the user's
machine. LibreBMC therefore intends to make a low-cost (dual) FPGA-based
"Experimentation" platform, as Libre/Open Hardware, for developers to
iteratively test out development of alternative BMC Firmware (LibreBMC,
OpenBMC, u-bmc), without risk of damage to the machine it is managing. One
FPGA will run LibreBMC, the other Libre-SOC/Microwatt/A2O, and the first
will boot the second. This will allow the next phase - actual booting
of servers and desktop machines - to proceed with confidence.

__Experience__: I am the TSC of the OpenPOWER Foundation, and I am
involved with Libre-SOC. I already build a full Linux Distribution,
PowerEL to several platforms. However this work would be spread over
several people in the company and within the Libre-SOC team.

__Amount__: 50000 EUR

__Use__:

- Design and fabrication of Libre/Open Hardware Dual FPGA Carrier boards
(most likely accepting ECP5 based devices such as the OrangeCrab module)
- Porting of both LibreBMC and OpenBMC to the FPGA Board (with optionally u-bmc)
- Porting to Raptor Engineering's Arctic Tern Board (Lattive ECP5 based FPGA board)
- Implementation of server side LPC (client-side already exists)
- Verilator simulation of both client and server side LPC and testing of the two simulations back-to-back

__Comparison__: There are no real open source BMC stack projects, there
is OpenBMC, u-bmc which is the software stack, there is an Arctic Tern
board, and there is an DC-SCM board of Antmicro, however there is no
real overall project that enables a user to pick up an hardware BMC and
put their their software on that BMC

__Challenges__: Making test boards, porting the software to those boards.

__Ecosystem__: As a result of our efforts it will make it easier for
other users to expand targets to existing hardware (this is not included
in this project, however it is the end goal). In the long term we want
manufacturers to make this a standard, as OCP NIC are becoming a standard
on servers, and USB-C is required by the EU, we want DC-SCM, RunBMC
modules to become standard (another proposal submitted for thiw work)


## NLNet proposal 2 : LibreBMC User Standard

__Abstract__:	LibreBMC replaces the proprietary Baseboard Management
Controller (BMC) and its secret hidden firmware, entirely. One of the most
commonly-used BMC Processors in the world has a silicon-baked plaintext
password for its Serial Console, and with a BMC being the very means by
which a processor's BIOS is uploaded, this publicly-available password
allows for a full bypass of every conceivable security measure. BMC
(out-of-band) Processors are also present in every AMD and Intel desktop
and laptop in the world, think the Intel Management Engine. Even replacing
the BIOS with coreboot is not enough to gain trust because the BMC is
in charge of uploading coreboot/oreboot, and could easily alter it. At
least in this case if the BMC's firmware is replaced it increases trust
that the payload (coreboot/oreboot) has not been tampered with. By using
FPGA based BMC, the software, hardware can be open sourced and provides
insight to the end-user, we want to make it easy for users to be able to
build their on BMC firmware using minimal technical knowledge. However
we will also need hardware to support that and part of this project is
to make LibreBMC based design a standard as the OCP Mezz NIC standard
is on servers, or SO-DIMM's or LPDDR on user devices, like laptops,
and desktops.

__Experience__: I am part of the LibreBMC project, the Libre-SOC project,
the PowerEL project and we want to get involved with OCP to push the
DC-SCM and RunBMC standard to go industry wide so the adoption becomes
easy and manufacturers provide this standard on all devices.

__Amount__: 50000 EUR

__Use__:

- Development of an EU Standard for Baseboard Management Control,
suitable for EU end-user products such as chromebooks, laptops, and
desktop computers (instead of the current soldered-down insecure ICs).
- Build LibreBMC images for end-users to download and flash easily -
Have a service to build own images for personal usage so people can
customize their BMC image - Be involved with OCP to steer the DC-SCM
and RunBMC standards

__Comparison__: There has been no effort to open the BMC to this point,
we started under the LibreBMC project backed by the OpenPOWER Foundation
and as POWER users we have several experimental systems that are becoming
available for testing use, those technical efforts need to be put to
benefit of the end-user and their devices, such as laptops, desktops,
servers and others.
__Challenges__: Making a standard BMC for manufacturers to be able,
willing and suggested to implement. Making users aware of the possibility
to customize BMC setup to be security and privacy aware.

__Ecosystem__:	We want to persuade the OCP to make it mandatory and
then convince the EU to also to that as they did with USB-C. We need
end-users to care about the open source hardware and software stack
running on a part most people do not even know exists, as it is hidden
from the user in most cases.


# NLNet draft
## draft work version prior to submission

Please be short and to the point in your answers; focus primarily on
the what and how, not so much on the why. Add longer descriptions as
attachments (see below). If English isn't your first language, don't
worry - our reviewers don't care about spelling errors, only about
great ideas. We apologise for the inconvenience of having to submit in
English. On the up side, you can be as technical as you need to be (but
you don't have to). Do stay concrete. Use plain text in your reply only,
if you need any HTML to make your point please include this as attachment.

## Abstract: Can you explain the whole project and its expected outcome(s).

LibreBMC replaces the proprietary Base board Management Controller (BMC) and its
secret firmware, entirely. In servers typically used in Data Centres
and for scenarios where data privacy is paramount
this turns out to be critical.  One of the most commonly-used BMC
Processors in the world has a silicon-baked plaintext password for its
Serial Console, and with a BMC being the very means by which
a processor's BIOS is uploaded, this publicly-available password
allows for a full bypass of every conceivable
security measure.

BMC Processors are also present in every AMD and Intel desktop and Laptop
in the world.  Even replacing the BIOS with coreboot is not enough to
gain trust because the BMC is in charge of uploading coreboot, and could
easily alter it.
At least in this case if the BMC's firmware is replaced it increases
trust that the payload (coreboot) has not been tampered with. However
this is so low-level that there is serious risk of damaging the user's
machine.

LibreBMC therefore intends to make a low-cost dual FPGA-based "Experimentation"
platform, as Libre/Open Hardware, for developers to iteratively
test out development of alternative BMC Firmware (LibreBMC, OpenBMC),
without risk of damage to the machine it is managing.  One FPGA will
run LibreBMC, the other Libre-SOC/Microwatt/A2O, and the first will boot
the second.

This will allow the next phase - actual booting of servers and desktop
machines - to proceed with confidence.

# Have you been involved with projects or organisations relevant to this project before? And if so, can you tell us a bit about your contributions?


# Requested Amount    

EUR 75,000.

# Explain what the requested budget will be used for? 

* Design and fabrication of Libre/Open Hardware Dual FPGA Carrier
  boards (most likely accepting OrangeCrab as a module)
* Porting of both LibreBMC and OpenBMC to the FPGA Board
* Porting to Raptor Engineering's Arctic Tern Board
* Implementation of *server* side LPC (client-side already exists)
* Verilator simulation of both client and server side LPC
  and testing of the two simulations back-to-back
* Development of an EU Standard for Baseboard Management Control,
  suitable for EU end-user products such as chromebooks, laptops,
  and desktop computers (instead of the current soldered-down
  insecure ICs).

# Compare your own project with existing or historical efforts.

TODO compare with RunBMC and OpenBMC.

## What are significant technical challenges you expect to solve during the project, if any?



## Describe the ecosystem of the project, and how you will engage with relevant actors and promote the outcomes?


# Extra info to be submitted

* TODO URLs etc
