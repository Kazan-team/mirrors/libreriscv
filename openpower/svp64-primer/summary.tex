\section*{Summary}
The proposed \acs{SV} is a Scalable Vector Specification for a hardware for-loop \textbf{that
ONLY uses scalar instructions}.

\begin{itemize}
\itemsep 0em
\item The Power \acs{ISA} v3.1 Spec is not altered.
  v3.1 Code-compatibility is guaranteed.
\item Does not require sacrificing 32-bit Major Opcodes.
\item Does not require adding duplicates of instructions
      (popcnt, popcntw, popcntd, vpopcntb, vpopcnth, vpopcntw, vpopcntd)
\item Fully abstracted: does not create Micro-architectural dependencies
      (no fixed "Lane" size), one binary works across all existing
      \textit{and future} implementations.
\item Specifically designed to be easily implemented
      on top of an existing Micro-architecture (especially
      Superscalar Out-of-Order Multi-issue) without
      disruptive full architectural redesigns.
\item Divided into Compliancy Levels to suit differing needs.
\item At the highest Compliancy Level only requires five instructions
      (SVE2 requires appx 9,000. \acs{AVX-512} around 10,000. \acs{RVV} around
      300).
\item Predication, often-requested, is added cleanly
      (without modifying the v3.1 Power ISA)
\item In-registers arbitrary-sized Matrix Multiply is achieved in three
      instructions (without adding any v3.1 Power ISA instructions)
\item Full \acs{DCT} and \acs{FFT} RADIX2 Triple-loops are achieved with
      dramatically reduced instruction count, and power consumption expected
      to greatly reduce. Normally found only in high-end \acs{VLIW} \acs{DSP}
      (TI MSP, Qualcomm Hexagon)
\item Fail-First Load/Store allows Vectorised high performance
      strncpy to be implemented in around 14
      instructions (hand-optimised \acs{VSX} assembler is 240).
\item Inner loop of MP3 implemented in under 100 instructions
      (gcc produces 450 for the same function on POWER9).
\end{itemize}

All areas investigated so far consistently showed reductions in executable
size, which as outlined in \cite{SIMD_HARM} has an indirect reduction in
power consumption due to less I-Cache/TLB pressure and also Issue remaining
idle for long periods.
Simple-V has been specifically and carefully crafted to respect
the Power ISA's Supercomputing pedigree.

\begin{figure}[hb]
    \centering
	\includegraphics[width=0.6\linewidth]{power_pipelines.png}
	\caption{Showing how SV fits in between Decode and Issue}
	\label{fig:power_pipelines}
\end{figure}

\pagebreak

\subsection*{What is SIMD?}

\acs{SIMD} is a way of partitioning existing \acs{CPU}
registers of 64-bit length into smaller 8-, 16-, 32-bit pieces.
\cite{SIMD_HARM}\cite{SIMD_HPC}
These partitions can then be operated on simultaneously, and the initial values 
and results being stored as entire 64-bit registers (\acs{SWAR}).
The SIMD instruction opcode
includes the data width and the operation to perform.
\par

\begin{figure}[hb]
    \centering
	\includegraphics[width=0.6\linewidth]{simd_axb.png}
	\caption{SIMD multiplication}
	\label{fig:simd_axb}
\end{figure}

This method can have a huge advantage for rapid processing of
vector-type data (image/video, physics simulations, cryptography,
etc.),
\cite{SIMD_WASM},
 and thus on paper is very attractive compared to
scalar-only instructions.
\textit{As long as the data width fits the workload, everything is fine}.
\par

\subsection*{Shortfalls of SIMD}
SIMD registers are of a fixed length and thus to achieve greater
performance, CPU architects typically increase the width of registers
(to 128-, 256-, 512-bit etc) for more partitions.\par Additionally,
binary compatibility is an important feature, and thus each doubling
of SIMD registers also expands the instruction set. The number of
instructions quickly balloons and this can be seen in for example
IA-32 expanding from 80 to about 1400 instructions since
the 1970s\cite{SIMD_HARM}.\par

Five digit Opcode proliferation (10,000 instructions) is overwhelming.
The following are just some of the reasons why SIMD is unsustainable as
the number of instructions increase:
\begin{itemize}
    \itemsep 0em
	\item Hardware design, ASIC routing etc.
	\item Compiler design
	\item Documentation of the ISA
	\item Manual coding and optimisation
	\item Time to support the platform
	\item Compilance Suite development and testing
	\item Protracted Variable-Length encoding (x86) severely compromises
	Multi-issue decoding
\end{itemize}

\subsection*{Scalable Vector Architectures}
An older alternative exists to utilise data parallelism - vector
architectures. Vector CPUs collect operands from the main memory, and
store them in large, sequential vector registers.\par

A simple vector processor might operate on one element at a time,
however as the element operations are usually independent,
a processor could be made to compute all of the vector's
elements simultaneously, taking advantage of multiple pipelines.\par

Typically, today's vector processors can execute two, four, or eight
64-bit elements per clock cycle.
\cite{SIMD_HARM}.
Vector ISAs are specifically designed to deal with (in hardware) fringe
cases where an algorithm's element count is not a multiple of the
underlying hardware "Lane" width. The element data width
is variable (8 to 64-bit just like in SIMD)
but it is the \textit{number} of elements being
variable under control of a "setvl" instruction that specifically
makes Vector ISAs "Scalable"
\par

\acs{RVV} supports a VL of up to $2^{16}$ or $65536$ bits,
which can fit 1024 64-bit words.
\cite{riscv-v-spec}.
The Cray-1 had 8 Vector Registers with up to 64 elements (64-bit each).
An early Draft of RVV supported overlaying the Vector Registers onto the
Floating Point registers, similar to \acs{MMX}.

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.6\linewidth]{cray_vector_regs.png}
	\caption{Cray Vector registers: 8 registers, 64 elements each}
	\label{fig:cray_vector_regs}
\end{figure}

Simple-V's "Vector" Registers (a misnomer) are specifically designed to fit
on top of
the Scalar (GPR, FPR) register files, which are extended from the default
of 32, to 128 entries in the high-end Compliancy Levels.  This is a primary
reason why Simple-V can be added on top of an existing Scalar ISA, and
\textit{in particular} why there is no need to add explicit Vector
Registers or
Vector instructions.  The diagram below shows \textit{conceptually}
how a Vector's elements are sequentially and linearly mapped onto the
\textit{Scalar} register file:

\begin{figure}[ht]
    \centering
	\includegraphics[width=0.6\linewidth]{svp64_regs.png}
	\caption{three instructions, same vector length, different element widths}
	\label{fig:svp64_regs}
\end{figure}

\pagebreak

\subsection*{Simple Vectorisation}
\acs{SV} is a Scalable Vector ISA designed for hybrid workloads (CPU, GPU,
VPU, 3D).  Includes features normally found only on Cray-style Supercomputers
(Cray-1, NEC SX-Aurora) and GPUs.  Keeps to a strict uniform RISC paradigm,
leveraging a scalar ISA by using "Prefixing".
\textbf{No dedicated vector opcodes exist in SV, at all}.
SVP64 uses 25\% of the Power ISA v3.1 64-bit Prefix space (EXT001) to create
the SV Vectorisation Context for the 32-bit Scalar Suffix.

\vspace{10pt}
Main design principles
\begin{itemize}
    \itemsep 0em
	\item Introduce by implementing on top of existing Power ISA
	\item Effectively a \textbf{hardware for-loop}, pauses main PC,
	      issues multiple scalar operations
	\item Strictly preserves (leverages) underlying scalar execution
          dependencies as if
	      the for-loop had been expanded into actual scalar instructions
          ("preserving Program Order")
	\item Augments existing instructions by adding "tags" - provides
          Vectorisation "context" rather than adding new opcodes.
	\item Does not modify or deviate from the underlying scalar
          Power ISA unless there's a significant performance boost or other
          advantage in the vector space
	\item Aimed at Supercomputing: avoids creating significant
	      \textit{sequential dependency hazards}, allowing \textbf{high
	      performance multi-issue superscalar microarchitectures} to be
          leveraged.
\end{itemize}

Advantages include:
\begin{itemize}
    \itemsep 0em
	\item Easy to create first (and sometimes only) implementation
	      as a literal for-loop in hardware, simulators, and compilers.
	\item Obliterates SIMD opcode proliferation
          ($O(N^6)$) as well as dedicated Vectorisation
          ISAs. No more separate vector instructions.
	\item Reducing maintenance overhead (no separate Vector instructions).
          Adding any new Scalar instruction
          \textit{automatically adds a Vectorised version of the same}.
	\item Easier for compilers, coders, documentation
\end{itemize}

