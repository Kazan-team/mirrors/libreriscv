[[!toc]]

# introduction

the purpose of this page is to create and evaluate alternative
encoding schemes that allow for a hybrid mixture of
(ultra-precious) 32-bit and 64-bit (actually `x86 REP`-like
prefixed) instructions.

a specific view to attaining high-speed massive-wide multi-issue decode
is a high priority.  therefore especially length-detection must be kept
brutally simple.

**All evaluations point towards the original encoding proposed in the
very first revision of [[rfc/ls001]] as it is clear that all other decoders
are far too complex**. [shortcut to 64-only PO9](./#sv_po9_ext2xx)

# **superseded** alternative 32-64 encoding (1)

conflict to resolve: EXT90x and EXT232. they are indistinguishable.

```
|0-5| 6-27 28 29|30-31|32|33-35|36-37| 38-59 | 60-63 | Description      |
|---|-----|-----|-----|--|-----|-----|-------|-------|------------------|
|PO9| rm0 | 0 0 | 0 0 | 0  000   rm1 |  xxxx | 0000  | SVP64:EXT900     |
|PO9| rm0 | 0 0 | 0 0 | 1  000   rm1 |  xxxx | 0000  | SSingle:EXT900   |
|PO9| xxx | x 0 | 0 0 | x !zero0     |  xxxx | !zero1| 55-bit RESERVED  |
|PO9| xxx | 0 1 | 0 0 |              32-bit EXT900 (Vectorizable)       |
|PO9| xxx | 1 1 | 0 0 |              32-bit EXT901 (Unvectorizable)     |
|PO9|   !ZERO   | 0 0 |  1 | DWi                   | SSingle:EXT232-263 |
|PO9|   0000    | 0 0 |  1 | DWi                   | Scalar EXT232-263  |
|PO9|   RM      | 1 0 |  1 | DWi                   | SVP64:EXT232-263   |
|PO9|   0000    | 0 1 | Defined Word-instruction   | 32-bit Unvec in 64b|
|PO9|   !ZERO   | 0 1 | Defined Word-instruction   | SSingle:EXT000-063 |
|PO9|   RM      | 1 1 | Defined Word-instruction   | SVP64:EXT000-063   |
```

Fields:

* `RM`: 24-bit SVP64 prefix
* `RM <- rm0 || rm1` split field
* `!zero0 || !zero1` a split field that must not be zero

Length detection:

```
    if PO1                   return 64
    elif not PO9             return 32
    elif Word[31] = 1        return 64
    elif Word[29:30] = 0b10  return 32
    elif Word[29:30] = 0b00  return 64
    else                     return 32
```

Instruction allocation restrictions:

* setvl is Unvectorizable but needs more space later: use 55-bit
* likewise svshape svindex and svshape2, all these need 32-bit
* svstep *is* Vectorizable but needs to be encoded 32-bit in order
  to reduce loop-size
* space for 32-bit instructions is needed: some instructions
  just make no sense if done as 64-bit because they **replace**
  two 32-bit instructions.
* but those types of space-saving instructions
  **also need to be Vectorizable**

# **superseded** alternative 32-64 encoding (2)

requires reducing SVP64Single to 23 bits. luckily there are 2 spare

the complexity of attempting to fit 32-bit instructions into
PO9 is very high. encoding (1) attempts to fit `setvl` etc. into
32-bit but it is very tight.  the entire EXT900 area would be
taken up by the SVP64 Management instructions, below, defeating
the purpose of having it.

* `setvl` - Unvectorizable, EXT0xx, `RT RA CTR SVSTATE`, 6-bit XO (including Rc=1)
* `psetvl` - Unvectorizable, EXT1xx, ditto
* `svstep` - Vectorizable, either EXT900 (very precious) or EXT1xx (better), `RT, SVSTATE`, 6-bit XO (including Rc=1)
* `svindex` - Unvectorizable, EXT0xx, `SVSTATE,SVSHAPE0-3`, 5-bit XO
* `psvindex` - Unvectorizable, EXT1xx, ditto
* `svstep(2)` - Unvectorizable, EXT0xx, `SVSTATE,SVSHAPE0-3`, 5-bit XO
* `svstep(2)` - Unvectorizable, EXT1xx, ditto

```
|0-5| 6-28|29 30 31|32|33-36|37 | 38-60 | 61-63 | Description      |
|---|-----|--------|--|-----|---|-------|-------|------------------|
|PO9| rm0 | 1  0 0 | 0 0000  rm1|  xxxx | 000   | SVP64:EXT900     |
|PO9|!ZERO| 1  0 0 | 0 1000  1  |  xxxx | 000   | SSingle:EXT900   |
|PO9| xxx | 1  0 0 | 0 !zero0   |  xxxx | !zero1| 55-bit RESERVED  |
|PO9| xxx | 0  0 0 |            32-bit EXT900                      |
|PO9|!ZERO| 1  0 0 |  1 | DWi                 | SSingle:EXT232-263 |
|PO9| 000 | 1  0 0 |  1 | DWi                 | Scalar EXT232-263  |
|PO9|  nnnn  | 1 0 |  1 | DWi                 | SVP64:EXT232-263   |
|PO9|  0000  | 0 1 | Defined Word-instruction | 32-bit Unvec in 64b|
|PO9|!ZERO   | 0 1 | Defined Word-instruction | SSingle:EXT000-063 |
|PO9|  nnnn  | 1 1 | Defined Word-instruction | SVP64:EXT000-063   |
```

Length detection:

```
    if PO1                   return 64
    elif not PO9             return 32
    elif Word[29:31] = 0b000 return 32
    else                     return 64
```

# **revoked** alternative hybrid 32-64 encoding (3)

aim of this idea is to attempt simplification of area identification
and length.  the 55-bit area is eliminated and may only be reintroduced
by sacrificing parts of EXT200-231, bear in mind that EXT240 is already
unavailable (used by EXT900).

```
|0-5| 6-29   |30 31|32 |33-37   | 38-61 | 62-63 | Description      |
|---|--------|-----|---|--------|-------|-------|------------------|
|PO9| xxxx   | 0 0 | (next new instruction)     |  32-bit EXT900   |
|PO9| 0000   | 1 0 | 0 | 10001  |  //// |  00   | RESERVED         |
|PO9| !ZERO  | 1 0 | 0 | 10001  |  xxxx |  00   | SSingle:EXT900   |
|PO9| SVRM   | 1 0 | 0 | 10001  |  xxxx |  01   | SVP64:EXT900     |
|PO9| ////   | 1 0 | 0 | 10001  |  //// |  10   | RESERVED         |
|PO9| ////   | 1 0 | 0 | 10001  |  //// |  11   | RESERVED         |
|PO9| !ZERO  | 1 0 | 0 | !PO9   |     xxxx      | SSingle:EXT232-263 |
|PO9|  0000  | 1 0 | 0 | !PO9   |     xxxx      | Scalar EXT232-263  |
|PO9|  SVRM  | 1 0 | 1 | !PO9   |     xxxx      | SVP64:EXT232-263   |
|PO9|  0000  | 0 1 | Defined Word-instruction   | 32-bit Unvec in 64b|
|PO9| !ZERO  | 0 1 | Defined Word-instruction   | SSingle:EXT000-063 |
|PO9|  SVRM  | 1 1 | Defined Word-instruction   | SVP64:EXT000-063   |
```

**Notes**:

* with `setvl` `svindex` `svshape` and `svshape2` being Unvectorizable Management
instructions requiring 6-bit `XO` and needing EXT1xx variants later, they are
best placed in EXT0xx.  EXT900 is Vectorizable and extremely precious.  Further
complication of Decode to create Vectorized EXT900 and Unvectorized EXT901
is inadviseable (rejected in alternative 1)
* EXT200-231 is intended to be partially-filled with LD/ST-Immediate-Postincrement
  and it is strongly recommended that the exact same Primary Opcode be
  used, matching the lower 5 bits. `lzbu` is EXT033, therefore `lzbup` is
  recommended to be EXT233. This will greatly simplify Decode.
* With no "overlapping" Encodings permitted on top of EXT232-263 (except
  EXT240) Speculative Decode may begin early in Multi-Issue systems safe
  that for the most part the 32-bit instruction is either going to be
  EXT0xx or EXT2xx, regardless of whether it is SVP64-Prefixed.
  LD/ST-Immediate-Postincrement discernment from
  LD/ST-Immediate is delayed, but Decode of other EXT232-263 operations
  are *also delayed* pending identification.
  

**Identification**:

The following identification-criteria may be easily verified by taking
a copy of the table above and striking out each line that fails each
test.  Only EXT000-063 is not by a "process of elimination" (if ... return True)

**Length detection**: (critically-important to be short)

```
    if PO1                   return 64
    elif not PO9             return 32
    elif Word[30:31] = 0b00  return 32
    else                     return 64
```

**EXT232-263:**

```
    if not PO9                return False    # eliminate EXT900
    if Word[30:31] == 0b00    return False    # eliminate EXT0xx
    if Word[31] == 0b1        return False    # eliminate EXT900 and reserved areas
    if Word[33:37] != 0b10001 return False
    return True
```

**EXT900:**

```
    # easy first step
    if not PO9                 return False
    # eliminate EXT0xx, 32-Unvec-in-64b
    if Word[31] = 0b1          return False
    # eliminate EXT2xx
    if Word[30:31] =  0b10 and
       Word[33:37] != 0b10001   return False
    # eliminate currently-RESERVED areas
    if Word[30:31] =  0b10 and
       Word[62]    =  0b1      return False
    # eliminate final RESERVED area
    if Word[30:31] =  0b10 and
       Word[62:63] =  0b00 and
       Word[6:29 ] =  0x000000 return False
    # last-remaining is EXT900
    return True
```

**EXT000-063**: (includes Prefixed)

```
    if PO1                     return False # or other reserved
    if not PO9                 return True  # prefixed needs more
    # eliminate EXT900
    if Word[30:31] =  0b00     return False
    # eliminate Prefixed-EXT900, RESERVED and EXT200
    if Word[31]    =  0b0      return False
    # eliminate 32-bit Unvec in 64b area
    if Word[30:31] =  0b01 and
       Word[6:29 ] =  0x000000 return False
    return True
```

**SVP64**:

```
    # easy-elimination, first
    if not PO9                 return False
    if Word[30]    =  0b0      return False
    # eliminate anything not SVP64:EXT900
    if Word[30:31] =  0b10 and
       Word[33:37] = 0b10001 and
       Word[62:63] != 0b01    return False
    # eliminate anything not SVP64:EXT200-231
    if Word[31:32] =  0b00    return False
    # all remaining combinations are SVP64:EXTnnn
    return True
```

**SVP64Single**:

```
    # easy-elimination, first
    if not PO9                 return False
    # eliminate 32-bit EXT900
    if Word[30:31] =  0b00     return False
    # eliminate SVP64:EXT000-063
    if Word[30:31] =  0b11     return False
    # eliminate anything "Identity"
    if  Word[6:29 ] =  0x000000 return False
    # eliminate SVP64:EXT200-231
    if Word[30:32] =  0b101     return False
    # eliminate anything not SSingle:EXT900
    if Word[30:31] =  0b10 and
       Word[33:37] = 0b10001 and
       Word[62:63] != 0b00      return False
    # everything left is SVP64Single
    return True
```

**RESERVED**: (no need to pass for further detailed decode)

```
    # easy-elimination, first
    if not PO9                 return False
    # eliminate EXT900
    if Word[30:31] =  0b00     return False
    # eliminate SVP64:EXT000-031
    if Word[30:31] =  0b11     return False
    # eliminate SSingle:EXT000-063
    if Word[6:29 ] != 0x000000 and
       Word[30:31]  =  0b01    return False
    # eliminate EXT200-231
    if Word[30:31] =  0b10 and
       Word[33:37] = 0b10001   return False
    # eliminate SSingle:EXT900
    if Word[30:31] =  0b10 and
       Word[6:29 ] != 0x000000 and
       Word[33:37] = 0b10001 and
       Word[62:63] = 0b00      return False
    # eliminate SVP64:EXT900
    if Word[30:31] =  0b10 and
       Word[33:37] = 0b10001 and
       Word[62:63] = 0b01      return False
    # all else needs further detailed decode
    return True
```

# alternative **no 32 encoding** 64-bit only (4) **current**  <a name="#sv_po9_ext2xx" />

this idea gets even simpler. the 55-bit area is eliminated and may only be
reintroduced by sacrificing parts of EXT200-263. however doing so **WILL** damage
high-performance decode.

advantages:

* brutally simple length detection (multi-issue 1 clock cycle)
* extremely easy SVP64 identification
* extremely easy SVP64Single identification


```
|0-5| 6-29   |30 31|32-37   | 38-663 | Description      |
|---|--------|-----|--------|--------|------------------|
|PO9| xxxx   | x x | 010001 |  ////  | RESERVED         |
|PO9| xxxx   | x x | 000001 |  ////  | RESERVED         |
|PO9| !ZERO  | 1 1 |  !PO9  |     xxxx        | SSingle:EXT200-263 |
|PO9|  0000  | 1 1 |  !PO9  |     xxxx        | Scalar EXT200-263  |
|PO9|  SVRM  | 1 0 |  !PO9  |     xxxx        | SVP64:EXT200-263   |
|PO9|  0000  | 0 1 | Defined Word-instruction | 32-bit EXT300-363  |
|PO9| !ZERO  | 0 1 | Defined Word-instruction | SSingle:EXT000-063 |
|PO9|  SVRM  | 0 0 | Defined Word-instruction | SVP64:EXT000-063   |
```
* bit 31 is "SVP64Single" if set else "SVP64"
* bit 30 is EXT200-263 if set otherwise EXT000-063

(note: this is one of the original early PO9 Encodings,
 proposed in [[rfc/ls001]] and rejected at the time:
 with hindsight it should not have been)

**Identification**:

The following identification-criteria may be easily verified by taking
a copy of the table above and striking out each line that fails each
test.  Only EXT000-063 is not by a "process of elimination" (if ... return True)

**Length detection**: (critically-important to be short - 1 clock cycle)

```
    if PO1 or PO9            return 64
    else                     return 32
```

**Scalar**: (includes EXT0xx, EXT2xx and EXT3xx):

|0-5| 6-29   |30 31|32-37   | 38-663 | Description      |
|---|--------|-----|--------|--------|------------------|
|PO9| xxxx   | x x | 010001 |  ////  | RESERVED         |
|PO9| xxxx   | x x | 000001 |  ////  | RESERVED         |
|PO9| !ZERO  | 1 1 |  !PO9  |     xxxx        | SSingle:EXT200-263 |
|PO9|  0000  | 1 1 |  !PO9  |     xxxx        | Scalar EXT200-263  |
|PO9|  SVRM  | 1 0 |  !PO9  |     xxxx        | SVP64:EXT200-263   |
|PO9|  0000  | 0 1 | Defined Word-instruction | 32-bit EXT300-363  |
|PO9| !ZERO  | 0 1 | Defined Word-instruction | SSingle:EXT000-063 |
|PO9|  SVRM  | 0 0 | Defined Word-instruction | SVP64:EXT000-063   |

```
    if PO1                    return True # EXT1xx is Scalar
    if not PO9                return True # anything not PO9 is EXT0xx
    if Word[6:29 ] = 0x000000 return True # EXT2xx and EXT3xx
    if Word[32:37] = 0b010001 return False # double-PO9 Reserved
    if Word[32:37] = 0b000001 return False # PO9-PO1 Reserved
    return False                          # all else is SVP64/SSingle
```

**EXT200-263:**

```
    if not PO9                return False    # eliminate EXT0xx
    if Word[31] == 0b1        return False    # remaining is EXT2xx
    return True
```



**EXT000-063**: (includes Prefixed)

```
    if PO1                     return False # or other reserved
    if not PO9                 return True  # prefixed needs more
    if Word[31]    =  0b0      return False # eliminate EXT2xx
    return True                             # remaining is EXT0xx
```

**SVP64**:

```
    if not PO9                 return False # easy-elimination, first
    if Word[31]    =  0b1      return False # eliminate anything not SVP64
    return True    # all remaining are SVP64:EXT0xx/EXT2xx
```

**SVP64Single**:

```
    if not PO9                 return False # easy-elimination, first
    if Word[31] =  0b0         return False # eliminate SVP64:*
    if Word[6:29 ] =  0x000000 return False # eliminate anything "Identity"
    return True    # everything left is SVP64Single

```

**RESERVED**: (no need to pass for further detailed decode)

```
    if not PO9                 return False # easy-elimination, first
    if Word[32:37] = 0b010001  return False # eliminate double-PO9
    if Word[32:37] = 0b000001  return False # eliminate PO9-PO1
    return True                # all else requires detailed decode
```
