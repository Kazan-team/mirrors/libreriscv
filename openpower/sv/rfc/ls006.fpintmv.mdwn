# RFC ls006 FPR <-> GPR Move/Conversion

* Funded by NLnet under the Privacy and Enhanced Trust Programme, EU
  Horizon2020 Grant 825310, and NGI0 Entrust No 101069594
* <https://libre-soc.org/openpower/sv/int_fp_mv/>
* <https://libre-soc.org/openpower/sv/rfc/ls006.fpintmv/>
* <https://bugs.libre-soc.org/show_bug.cgi?id=1015>
* <https://git.openpower.foundation/isa/PowerISA/issues/todo>

**Severity**: Major

**Status**: New

**Date**: 09 Feb 2024 v2

**Target**: v3.2B

**Source**: v3.1B

**Books and Section affected**: **UPDATE**

* Book I 4.6.5 Floating-Point Move Instructions
* Book I 4.6.7.2 Floating-Point Convert To/From Integer Instructions
* Appendix E Power ISA sorted by opcode
* Appendix F Power ISA sorted by version
* Appendix G Power ISA sorted by Compliancy Subset
* Appendix H Power ISA sorted by mnemonic

**Summary**

Single-precision Instructions added:

* `mffprs` -- Move From FPR Single
* `mtfprs` -- Move To FPR Single
* `ctfprs` -- Convert To FPR Single

Identical (except Double-precision) Instructions added:

* `mffpr` -- Move From FPR
* `mtfpr` -- Move To FPR
* `cffpr` -- Convert From FPR
* `ctfpr` -- Convert To FPR

**Submitter**: Luke Leighton (Libre-SOC)

**Requester**: Libre-SOC

**Impact on processor**:

* Addition of three new Single-Precision GPR-FPR-based instructions
* Addition of four new Double-Precision GPR-FPR-based instructions

**Impact on software**:

* Requires support for new instructions in assembler, debuggers,
  and related tools.

**Keywords**:

```
    GPR, FPR, Move, Conversion, ECMAScript, Saturating
```

**Motivation**

CPUs without VSX/VMX lack a way to efficiently transfer data between
FPRs and GPRs, they need to go through memory, this proposal adds more
efficient data transfer (both bitwise copy and Integer <-> FP conversion)
instructions that transfer directly between FPRs and GPRs without needing
to go through memory.

IEEE 754 does not specify what results are obtained when converting a NaN
or out-of-range floating-point value to integer: consequently, different
programming
languages and ISAs have made different choices, making binary portability
very difficult.  Below is an overview
of the different variants, listing the languages and hardware that
implements each variant.

**Notes and Observations**:

* These instructions are present in many other ISAs.
* ECMAScript rounding as one instruction saves 32 scalar instructions
  including seven branch instructions.
* Both sets are orthogonal (no difference except being Single/Double).
  This allows IBM to follow the pre-existing precedent of allocating
  separate Major Opcodes (PO) for Double-precision and Single-precision
  respectively.

**Changes**

Add the following entries to:

* Book I 4.6.5 Floating-Point Move Instructions
* Book I 4.6.7.2 Floating-Point Convert To/From Integer Instructions
* Book I 1.6.1 and 1.6.2

----------------

\newpage{}

[[!inline pages="openpower/sv/int_fp_mv/cvt_fp_to_int_overview" raw=yes ]]

----------

\newpage{}

[[!inline pages="openpower/sv/int_fp_mv/moves_and_conversions" raw=yes ]]

----------

\newpage{}

# Instruction Formats

Add the following entries to Book I 1.6.1.19 XO-FORM:

```
    |0    |6   |11  |13   |16   |21  |22  |31  |
    | PO  | RT | IT | CVM | FRB | OE | XO | Rc |
```

Add the following entries to Book I 1.6.1.15 X-FORM:

```
    |0   |6    |11  |13  |16   |21  |31  |
    | PO | FRT | IT | // | RB  | XO | Rc |
    | PO | FRT | //      | RB  | XO | Rc |
    | PO | RT  | //      | FRB | XO | Rc |
```

# Instruction Fields

Add XO to FRB's Formats list in Book I 1.6.2 Word Instruction Fields.

Add XO to FRT's Formats list in Book I 1.6.2 Word Instruction Fields.

Add new fields:

```
    IT (11:12)
        Field used to specify integer type for FPR <-> GPR conversions.

        Formats: X, XO

    CVM (13:15)
        Field used to specify conversion mode for
        integer -> floating-point conversion.

        Formats: XO
```

----------

\newpage{}

# Appendices

    Appendix E Power ISA sorted by opcode
    Appendix F Power ISA sorted by version
    Appendix G Power ISA sorted by Compliancy Subset
    Appendix H Power ISA sorted by mnemonic

|Form| Book | Page | Version | mnemonic | Description |
|----|------|------|---------|----------|-------------|
|VA  | I    | #    | 3.2B    |todo   | |

----------------

[[!tag opf_rfc]]
