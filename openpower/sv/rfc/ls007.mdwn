# RFC ls007 Ternary/Binary GPR and CR Field bit-operations

* Funded by NLnet under the Privacy and Enhanced Trust Programme, EU
  Horizon2020 Grant 825310, and NGI0 Entrust No 101069594
* <https://libre-soc.org/openpower/sv/bitmanip/>
* <https://libre-soc.org/openpower/sv/rfc/ls007/>
* <https://bugs.libre-soc.org/show_bug.cgi?id=1017>
* <https://git.openpower.foundation/isa/PowerISA/issues/117>

**Severity**: Major

**Status**: new

**Date**: 20 Oct 2022, 1st draft submitted 2023mar22

**Target**: v3.2B

**Source**: v3.1B

**Books and Section affected**: **UPDATE**

* Book I 2.5.1 Condition Register Logical Instructions
* Book I 3.3.13 Fixed-Point Logical Instructions
* Appendix E Power ISA sorted by opcode
* Appendix F Power ISA sorted by version
* Appendix G Power ISA sorted by Compliancy Subset
* Appendix H Power ISA sorted by mnemonic

**Summary**

Instructions added

* `ternlogi` -- GPR Ternary Logic Immediate
* `crternlogi` -- Condition Register Field Ternary Logic Immediate
* `binlog` -- GPR Dynamic Binary Logic
* `crbinlog` -- Condition Register Field Dynamic Binary Logic

**Submitter**: Luke Leighton (Libre-SOC)

**Requester**: Libre-SOC

**Impact on processor**:

* Addition of two new GPR-based instructions
* Addition of two new CR-field-based instructions

**Impact on software**:

* Requires support for new instructions in assembler, debuggers,
  and related tools.

**Keywords**:

```
GPR, CR-Field, bit-manipulation, ternary, binary, dynamic, look-up-table (LUT), FPGA, JIT
```
**Motivation**

* `ternlogi` is similar to existing `and`/`or`/`xor`/etc. instructions, but
  allows any arbitrary 3-input 1-output bitwise operation. This can be used to
  combine several instructions into one. E.g. `A ^ (~B & (C | A))` can become
  one instruction. This can also be used to have one instruction for
  bitwise MUX `(A & B) | (~A & C)`.
* `binlog` is like `ternlogi` except it supports any arbitrary 2-input
  1-output bitwise operation, where the operation can be selected dynamically
  at runtime. This operates similarly to a LUT in a FPGA.
* `crternlogi` is like `ternlogi` except it works with CRs instead of GPRs.
* `crbinlog` is like `binlog` except it works with CRs instead of GPRs. Likewise it
   is similar to a LUT in an FPGA.
* Combined these instructions save on instruction count and also help accelerate
  AI and JIT runtimes.

**Notes and Observations**:

* `ternlogi` is like the existing `xxeval` instruction, except operates
  on GPRs instead of VSRs and does not require VSX/VMX. SFS and SFFS are
  comparatively compromised.
* SVP64/VSX may have different semantics from SVP64/SFFS. SVP64
  orthogonality is compromised by a *non*-Vector-considerate argument
  that if equivalent  instructions are in VSX they do not have to be
  added to SFFS: they do.
* `crternlogi` is similar to the group of CR Operations (crand, cror etc)
  which have been identified as a Binary Lookup Group, except an 8-bit
  immediate is used instead of a 4-bit one, and up to 4 bits of a CR Field
  may be computed at once, saving at least 3 groups of CR operations.
* `crbinlut` is similar to the Binary Lookup Group of CR Operations
  except that the 4-bit lookup table comes from a CR Field instead of
  from an Immediate. Also like `crternlogi` up to 4 bits may be computed
  at once.

**Changes**

Add the following entries to:

* Book I 2.5.1 Condition Register Logical Instructions
* Book I 3.3.13 Fixed-Point Logical Instructions
* Book I 1.6.1 and 1.6.2

----------------

\newpage{}

[[!inline pages="openpower/sv/tern_bin" raw=yes ]]

# Forms

## CRB-FORM

Add the following section to Book I 1.6.1

```

    |0   |6   |9    |11   |14   |16   |19   |26  |31   |
    | PO | BF | msk | BFA | msk | BFB | TLI | XO | TLI |
    | PO | BF | msk | BFA | msk | BFB | //  | XO | /   |
```

## TLI-FORM

Add the following section to Book I 1.6.1

```
    |0   |6   |11  |16  |21   |29  |31  |
    | PO | RT | RA | RB | TLI | XO | Rc |
```

## VA-FORM

Add the following entry to VA-FORM in Book I 1.6.1.12

```
    |0      |6     |11     |16     |21     |26|27   |
    | PO    |  RT  |   RA  |   RB  |    RC |nh| XO  |
```

## Word Instruction Fields

Add the following to Book I 1.6.2

```
msk (9:10,14:15)
    Field used by crternlogi and crbinlut to decide which CR Field bits to
    modify.
    Formats: CRB
nh (26)
    Nibble High. Field used by binlog to decide if the look-up-table should
    be taken from bits 60:63 (nh=0) or 56:59 (nh=1) of RC.
    Formats: VA
TLI (21:28)
    Field used by the ternlogi instruction as the
    look-up table.
    Formats: TLI
TLI (21:25,19:20,31)
     Field used by the crternlogi instruction as the
     look-up table.
     Formats: CRB
```

* Add `TLI` to the `Formats:` list of all of `RA`, `RB`, `RT`, and `Rc`.
* Add `CRB` to the `Formats:` list of all of `BF`, `BFA`, `BFB`, and `BFC`.
* Add `TLI` to the `Formats:` list of `XO (29:30)`.
* Add `CRB` to the  `Formats:` list of `XO (26:31)`.
* Add `VA` to the  `Formats:` list of `XO (27:31)`.

----------

# Appendices

    Appendix E Power ISA sorted by opcode
    Appendix F Power ISA sorted by version
    Appendix G Power ISA sorted by Compliancy Subset
    Appendix H Power ISA sorted by mnemonic

|Form| Book | Page | Version | mnemonic   | Description |
|----|------|------|---------|------------|-------------|
|TLI | I    | #    | 3.2B    | ternlogi   | GPR Ternary Logic Immediate |
|VA  | I    | #    | 3.2B    | binlog     | GPR Binary Logic            |
|CRB | I    | #    | 3.2B    | crternlogi | CR Field Ternary Logic Immediate |
|CRB | I    | #    | 3.2B    | crbinlog   | CR Field Binary Logic |

[[!tag opf_rfc]]

----------------

