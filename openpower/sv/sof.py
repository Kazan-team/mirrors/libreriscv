def sof(RA, mask=None, zero=False):
    RT = RA if mask is not None and not zero else 0
    i = 0
    # start setting if no predicate or if 1st predicate bit set
    setting_mode = mask is None
    found = False
    while i < 16:
        bit = 1<<i
        if not setting_mode and mask is not None and (mask & bit):
            setting_mode = True # back into "setting" mode
            found = False       # start finding first
        if setting_mode:
            if mask is not None and not (mask & bit):
                setting_mode = False
            elif RA & bit and not found: # found a bit: set if not found
                RT |= bit
                found = True # don't set another bit
        i += 1
    return RT

if __name__ == '__main__':
     m  = 0b11000011
     v3 = 0b11010100 # vmsof.m v2, v3
     v2 = 0b01000000 # v2
     RT = sof(v3, m, True)
     print(bin(v3), bin(v2), bin(RT))
     v3 = 0b10010100 # vmsof.m v2, v3
     v2 = 0b00000100 # v2 contents
     RT = sof(v3)
     print(bin(v3), bin(v2), bin(RT))
     v3 = 0b10010101 # vmsof.m v2, v3
     v2 = 0b00000001 # v2
     RT = sof(v3)
     print(bin(v3), bin(v2), bin(RT))
     v3 = 0b00000000 # vmsof.m v2, v3
     v2 = 0b00000000 # v2
     RT = sof(v3)
     print(bin(v3), bin(v2), bin(RT))
