# Meeting at Southampton Premier Inn 2023oct18

Purpose: Complete some NLnet grant work. continuation of [[southampton_2023.oct.18]]

Attendees: Shriya, Luke, Andrey

* OPF ISA Grant changelist
* Ongoing grant readthrough
* SVP64 cookbook writing (part of OPF ISA)
* in-order model

## 10am

* Luke: writing notes for day

## 1030am

* Luke: completing bigint analysis for [[rfc/ls003]] <https://bugs.libre-soc.org/show_bug.cgi?id=1029>
* Andrey: comparing OPF ISA for MoU Schedule A update with Bob at NLnet

[[!tag meeting2023]]
