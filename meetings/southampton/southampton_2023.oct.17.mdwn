# Meeting at Frobisher House 2023oct17

Purpose: Complete some NLnet grant work.

Attendees: Shriya, Luke, Nicholas, Andrey, James

* Post-increment load/store <https://bugs.libre-soc.org/show_bug.cgi?id=1048>
* SVP64 cookbook
* in-order model

## 10am

* Andrey: Matrix cookbook. <https://bugs.libre-soc.org/show_bug.cgi?id=701>
* Shriya: pifixedstore.mdwn english
* Luke: pifpload.mdwn (pseudocode) and pifpstore.mdwn

## 10:30am

* Andrey: Matrix cookbook
* Shriya, Luke: Simulator introduction (on matrix)

## 12:00 break

## 13:00

* James, Andrey, Shriya, Luke, Nicholas
* Andrey: Matrix multiply example
* Shriya: pifixedload.mdwn- english descriptions and instruction names
* James: Status update (RED business, bug budget review with andrey)
* luke: Review 2nd phase OPF ISA WG RFCs <https://bugs.libre-soc.org/show_bug.cgi?id=1012>

## 13:45

* james: bug budget review with andrey
* shriya, nicholas: Post-Increment load/store review
* luke: RFP for RED bug #1012 
* Luke: suggest budget allocation for bug #772 <https://bugs.libre-soc.org/show_bug.cgi?id=772>

## 15:00

NGI Search meeting

## 16:00

* nicholas: continue review of pifixedload.mdwn
* luke: assisting nicholas

# 17:00

end

[[!tag meeting2023]]
