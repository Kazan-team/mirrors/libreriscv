# Wednesday 8th November 09:00 UTC

* Previous week's notes: NA
* Yesterday's notes: [[meetings/sync_up/sync_up_2023-11-07]]
* Next week's notes: [[meetings/sync_up/sync_up_2023-11-14]]

## Cesar

- nextpnr-xilinx has issues with 2.5V I/O.
  - FPGA split I/O split into banks, each bank has its own voltage.
  - For now ignore switches/LEDs, test UART as it's at 3.3V
(and that's enough for Libre-SOC).
  - Need to make a bug report in upstream nextpnr-xilinx.

- LD/ST CompUnit (CU) formal verification:
  - During test, CompUnit communicates with scoreboard and registerfiles.
  - Issue instruction to CU, fetch operands, store in reg's, send to ALU,
store result to regfile.
  - Put counters for those tests. Counter values must match
(fetch reg's only once, read ALU only *after* operands have been written).

- FOSDEM:
  - Suggested people to invite:
    - [Matt Venn](https://www.mattvenn.net/)
([Zero to ASIC](https://www.zerotoasiccourse.com/) course author)
    - Mohamed Kassem ([e-fabless](https://efabless.com/))
  - Shouldn't make all about LibreSOC (since we were lucky to get a devroom,
should also make the space available to other projects in the same area).

[[!tag meeting2023]]
[[!tag meeting_sync_up]]
