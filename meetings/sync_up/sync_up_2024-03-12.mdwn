# Tuesday 12th March 17:00 UTC

* Previous notes: [[meetings/sync_up/sync_up_2024-03-05]]
* Next week's notes: [[meetings/sync_up/sync_up_2024-03-19]]

# Main Agenda

Meeting notes:

* Outstanding Cryptorouter tasks:
  - Poly1305, [bug #1157](https://bugs.libre-soc.org/show_bug.cgi?id=1157)
  [bug #1158](https://bugs.libre-soc.org/show_bug.cgi?id=1158),
  [bug #1159](https://bugs.libre-soc.org/show_bug.cgi?id=1159)
  - Ed25519,
  [bug #1151](https://bugs.libre-soc.org/show_bug.cgi?id=1151),
  [bug #1166](https://bugs.libre-soc.org/show_bug.cgi?id=1166),
  [bug #1167](https://bugs.libre-soc.org/show_bug.cgi?id=1167)

everybody updated "paid" date for
[2022-08-107 bug #961](https://bugs.libre-soc.org/show_bug.cgi?id=961)

# Cesar

Nexys Video FPGA card arrived.
[bug #1004](https://bugs.libre-soc.org/show_bug.cgi?id=1004) will
do nmigen-boards pinout. will do a loopback and a demo.
also focussing on
[bug #1036](https://bugs.libre-soc.org/show_bug.cgi?id=1036)

suggests funding to tobias
[bug #990](https://bugs.libre-soc.org/show_bug.cgi?id=990)

also got a touchscreen for orangecrab (cool!)

# Jacob

updated bugzilla after getting paid for RFP for NLnet.2022-08-107.ongoing

# Luke

not able to do very much due to severe PTSD, 4 visits to A&E in one week,
and 3 doctor's appointments.

svshape4 for bigmul
[bug #1155](https://bugs.libre-soc.org/show_bug.cgi?id=1155)
working on curve25519_mul. svshape4 redesign needed. still working on it.

# Dmitry

submitting (new) binutils grant **as individual**
[bug #1259](https://bugs.libre-soc.org/show_bug.cgi?id=1259)
Dmitry becomes the Project Lead / Adminstrator.
Submitted: 2024-04-072

Asked about
[bug #1150](https://bugs.libre-soc.org/show_bug.cgi?id=1150).
Explain that draft_opcode_tables is the "canonical" file for
opcodes **before** doing CSV files.

* comments from jacob:
    * the table I've been using for fp transcendental ops
      <https://libre-soc.org/openpower/power_trans_ops/>
 
    * program for syncing parts of that table 
    <https://libre-soc.org/openpower/power_trans_ops_copy_from_PO59_table.py/>
 
    * it copies from the 2d grid to the tables lower down 

# Rita

attending a research methods course.

[[!tag meeting2024]]
[[!tag meeting_sync_up]]

