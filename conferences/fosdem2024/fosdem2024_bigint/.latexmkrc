$pdf_mode = 1;
$pdflatex = 'pdflatex --shell-escape %O %S';
add_cus_dep('dia', 'dia-tex', 0, 'dia2tex');
sub dia2tex {
    system("dia -n -t pgf-tex -e \"$_[0].dia-tex\" \"$_[0].dia\"");
    # replace labels with text `latex:\something` by the latex command `\something`
    my $sed_cmd = substr(<<'EOS', 0, -1);  # remove trailing newline
sed -i 's/^\\node\[[^\]*\] at ([-0-9.]\+\\du,[-0-9.]\+\\du){latexcmd:\(.*\)};$/latex:\1/
s/latex:\\ensuremath{\\backslash}/\\/g
s/latex:\\\([$_{}]\)/\1/g
'
EOS
    system("$sed_cmd \"$_[0].dia-tex\"");
}
$cleanup_includes_cusdep_generated = 1;
$max_repeat = 15;
