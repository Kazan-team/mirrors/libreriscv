# while (i<n)
setvl 2,0,4,0,1,1     # set MVL=4,VL=MIN(MVL,CTR)
#  while (i<n and a[i]<=m) : i += 1
sv.cmp/ff=gt/m=ge *0,0,*10,4 # truncates VL to min
sv.creqv *16,*16,*16  # set mask on already-tested
setvl 2,0,4,0,1,1     # set MVL=4,VL=MIN(MVL,CTR)
mtcrf 128,0           # clear CR0 (in case VL=0?)
#  while (i<n and a[i]>m):
sv.minmax./ff=le/m=ge/mr 4,*10,4,1 # r4 accumulate
crternlogi 0,1,2,127  # test >= (or VL=0)
sv.crnand/m=lt/zz *19,*16,0 # SO=~LT, if CR0.eq=0
#   nm = i: count masked bits. could use crweirds
sv.svstep/mr/m=so 1,0,6,1 # get vector dststep
sv.creqv *16,*16,*16  # set mask on already-tested
bc 12,0,-0x3c         # CR0 lt clear, branch back
